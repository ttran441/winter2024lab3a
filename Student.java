public class Student {
	
	public String name;
	public int averageGrade;
	public String id;
	public String program;
	public double height;
	
	public void sleepInClass() {
		
		System.out.println(this.name + " is sleeping in class!");
		
	}
	
	public void logInToComputer() {
		
		System.out.println(this.name + " of the " + this.program + " program is logging in to the the school computer!" +
		" They input " + this.id + " and their password.");
		
	}
	
}