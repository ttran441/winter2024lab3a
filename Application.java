public class Application {
	
	public static void main(String[] args) {
		
		Student andy = new Student();
		andy.name = "Andy Ionita";
		andy.averageGrade = 100;
		andy.id = "2333068";
		andy.program = "Gender Studies";
		andy.height = 1.0;
		
		Student april = new Student();
		april.name = "April Katsaros";
		april.averageGrade = 101;
		april.id = "2339186";
		april.program = "Computer Science";
		april.height = 1.6256;
		
		System.out.println("Andy fields");
		
		System.out.println(andy.name);
		System.out.println(andy.averageGrade);
		System.out.println(andy.id);
		System.out.println(andy.program);
		System.out.println(andy.height);
		
		System.out.println("                 ");
		
		System.out.println("April fields");
		
		System.out.println(april.name);
		System.out.println(april.averageGrade);
		System.out.println(april.id);
		System.out.println(april.program);
		System.out.println(april.height);
		
		System.out.println("                 ");
		
		andy.sleepInClass();
		andy.logInToComputer();
		
		System.out.println("                 ");
		
		april.sleepInClass();
		april.logInToComputer();
		
		Student[] section4 = new Student[3];
		section4[0] = andy;
		section4[1] = april;
		section4[2] = new Student();
		
		section4[2].name = "Tin Tran";
		section4[2].averageGrade = 22;
		section4[2].id = "6819332";
		section4[2].program = "Architecture";
		section4[2].height = 0.9;
		
		System.out.println("                 ");
		
		System.out.println(section4[2].name);
		System.out.println(section4[2].averageGrade);
		System.out.println(section4[2].id);
		System.out.println(section4[2].program);
		System.out.println(section4[2].height);
	}
	
}